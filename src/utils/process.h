#ifndef _PROCESS_H
#define _PROCESS_H

#include <stdio.h>
#include <sys/types.h>

FILE * popen2(char* const argv[], char* const envp[], char type, pid_t* pid);
int pclose2(FILE * fp, pid_t pid);

#endif