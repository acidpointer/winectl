#include "process.h"

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

// Thanks to: https://stackoverflow.com/questions/26852198/getting-the-pid-from-popen

#define PIPE_READ 0
#define PIPE_WRITE 1

FILE * popen2(char* const argv[], char* const envp[], char type, pid_t* pid)
{
    pid_t child_pid;
    int fd[2];
    pipe(fd);

    if((child_pid = fork()) == 0)
    {
        if (type == 'r')
        {
            close(fd[PIPE_READ]);               //Close the READ end of the pipe since the child's fd is write-only
            dup2(fd[PIPE_WRITE], PIPE_WRITE);   //Redirect stdout to pipe
            dup2(STDOUT_FILENO, STDERR_FILENO); // Redirect stderr to stdout
        }
        else
        {
            close(fd[1]);    //Close the WRITE end of the pipe since the child's fd is read-only
            dup2(fd[0], 0);   //Redirect stdin to pipe
        }

        setpgid(child_pid, child_pid); //Needed so negative PIDs can kill children of /bin/sh

        execve(argv[0], argv, envp);
        exit(0);        
    } else if (child_pid > 0) {
        if (type == 'r') {
            close(fd[PIPE_WRITE]); //Close the WRITE end of the pipe since parent's fd is read-only
        } else {
            close(fd[PIPE_READ]); //Close the READ end of the pipe since parent's fd is write-only
        }
    } else {
        perror("fork");
        exit(1);
    }

    *pid = child_pid;

    if (type == 'r')
    {
        return fdopen(fd[PIPE_READ], "r");
    }

    return fdopen(fd[PIPE_WRITE], "w");
}

int pclose2(FILE * fp, pid_t pid)
{
    int stat;

    fclose(fp);
    while (waitpid(pid, &stat, 0) == -1)
    {
        if (errno != EINTR)
        {
            stat = -1;
            break;
        }
    }

    return stat;
}