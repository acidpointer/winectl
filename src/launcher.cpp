#include "launcher.hpp"

#include <cstdlib>
#include <cstring>
#include <stdio.h>
#include <sys/types.h>

extern "C" {
    #include "utils/process.h"
}

Launcher::Launcher() {
    printf("Launcher constructor\n");
}

Launcher::~Launcher() {
    this->status = pclose2(this->proc_stream, this->pid);
    printf("Launcher process status: %d\n", this->status);
}


void Launcher::launch(char* const argv[], char* const envp[]) {

    this->proc_stream = popen2(argv, envp, 'r', &this->pid);
    
    char buff[4096];
    while (fgets(buff, sizeof(buff), this->proc_stream) != NULL)
        printf("STDOUT: %s", buff);

    printf("Launcher\n");
}

