#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <stdio.h>
#include <string>
#include <sys/types.h>

class Launcher {

private:
  FILE *proc_stream;
  int status;
  pid_t pid;

  char* command;

public:
  void launch(char* const argv[], char* const envp[]);
  Launcher();
  ~Launcher();
};

#endif